	var map;
	var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
	var newarr =[];
	function initMap() {
		const config = {
	    apiKey: "AIzaSyCo1ygAmv6nGkAZezfuNEoLKYO0AfOjEck",
	    authDomain: "track-1a018.firebaseapp.com",
	    databaseURL: "https://track-1a018.firebaseio.com",
	    projectId: "track-1a018",
	    storageBucket: "track-1a018.appspot.com",
	    messagingSenderId: "861172594169"
	  };
	  firebase.initializeApp(config);
	  var database = firebase.database();
	  var locationsRef = database.ref().child('Users');
	  

	  locationsRef.on('child_added', function(snapshot) {
	      //Do something with the data
			
		    var dummy = snapshot.val();
			  
			 // console.log('users:' + JSON.stringify(snapshot.key));
			 // console.log('val' + JSON.stringify(snapshot.val()));
			console.log('numChildren:' + JSON.stringify(snapshot.numChildren()));

			var markerIconAndPathColor = getRandomColor();

			snapshot.forEach(function(item){


		    var mylat= item.val().latitude;
			var mylong =item.val().longitude;
			  
			var items = Object.keys(item);
					
			var infowindow = new google.maps.InfoWindow({});
			var flightPlanCoordinates = [];
		    var bounds = new google.maps.LatLngBounds();
			//console.log(bounds);

			var myLatLng = {lat: mylat, lng: mylong };
			newarr.push(myLatLng);
			//console.log(newarr);
			
			var marker, i,map;
				
			map = new google.maps.Map(document.getElementById('map'), {
				center: myLatLng,
				zoom: 16,
				title: mylat,
				mapTypeId: google.maps.MapTypeId.ROADMAP

			});

			for(var i=0;i<newarr.length;i++){

				marker = new google.maps.Marker({
					position: new google.maps.LatLng(newarr[i].lat, newarr[i].lng),
					map: map,
					icon: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|"+markerIconAndPathColor.replace("#", ""),
					id:snapshot.key
				});

				flightPlanCoordinates.push(marker.getPosition());
		    	bounds.extend(marker.position);
				google.maps.event.addListener(marker, 'click', (function (marker, i) {
					return function () {
						infowindow.setContent("<ul><li>Latitude " + snapshot.key + "</li><li>Longitude " + newarr[i].lng + "</li></ul>");
						infowindow.open(map, marker);
					}
				})(marker, i));
				
			}
		    //console.log(flightPlanCoordinates);
			map.fitBounds(bounds);


			console.log('markercolor:'+markerIconAndPathColor);
			var flightPath = new google.maps.Polyline({
		      map: map,
		      path: flightPlanCoordinates,
		      strokeColor: markerIconAndPathColor,
		      strokeOpacity: 1.0,
		      strokeWeight: 2
		    });
		});

		flightPlanCoordinates =[];

	   });
		
	}

	function getRandomColor() {
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
	    color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
	}

